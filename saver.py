import numpy
import struct

def saveMatrix(mat,fName, dtype=numpy.single):
	with open(fName, 'wb') as file:
		file.write(mat.astype(dtype).transpose().tobytes())
	file.closed

def saveScalar(x,fName, dtype=numpy.single):
	with open(fName, 'wb') as file:
		if dtype == numpy.single:
			x_struct = struct.pack('=f',x)
		elif dtype == numpy.double or dtype == float:
			x_struct = struct.pack('=d',x)
		else:
			raise TypeError('Data type must be either single or double precision float.')
		file.write(x_struct)
	file.closed

def readScalar(fName, dtype=numpy.single):
	with open(fName, 'rb') as file:
		if dtype == numpy.single:
			raw_data = file.read(4)
			scalar = struct.unpack('=f', raw_data)
		elif dtype == numpy.double or dtype == float:
			raw_data = file.read(8)
			scalar = struct.unpack('=d', raw_data)
		else:
			raise TypeError('Data type must be either single or double precision float.')
		return scalar[0]
	file.closed