# Web interface for Python
[index](../README.md) \> [web interface](readme.md)

This Python-based web interface allows you to control AcouMan from a web browser.
This readme contains a guide on [installation](#installation), a quick [user guide](#user-guide), and a detailed [reference guide](#reference-guide).

## Installation

1. Clone this repository into the Raspberry Pi's home folder.
```bash
cd /home/pi/
git clone https://gitlab.fel.cvut.cz/aa4cc/acouman/webinterface.git
```
2. Build the Simulink models `RGBballs`, `triangles2`, and `triangles3`.
The repository with Simulink models is available at <https://gitlab.fel.cvut.cz/aa4cc/acouman/simulink-controller>.
Locate the path with the generated executable files (the default path should be `/home/pi/MATLAB_ws/your matlab version/`).
3. Look at the files named `RGBballs.service`, `triangles2.service`, and `triangles3.service` in this repository.
Make sure that the path specified in `ExecStart` matches the path to the generated executables.
If not, then either change the field in `ExecStart` or move the executables to the correct folder.
4. Copy all the `service` files to `/lib/systemd/system/`.
```bash
sudo cp *.service /lib/systemd/system/
```
5. Copy all the `JSON` files to `/home/pi/`.
```bash
cp *.json /home/pi/
```
6. Clone the `raspi-ballpos` repository into the Raspberry Pi's home folder,
```bash
cd /home/pi/
git clone https://github.com/aa4cc/raspi-ballpos.git
```
and follow the installation guide provided in the repository.
7. _Optional: enable the services to make them start during booting._
```bash
sudo systemctl enable vision
sudo systemctl enable webinterface
```

## User guide

Follow these steps in order to use AcouMan.
If something doesn't work, check the [troubleshooting](#troubleshooting) section.

We recommend filling in these login credentials in case you need to access your Raspberry Pi:

IP address: ......................................

User name: .................................

Password: .................................

### Turning it on

1. Plug in the Raspberry Pi power adapter.
Turn on the lab bench power supply, make sure that the output voltage is set to 16 V and the current limit is set to 2.5 A, and turn on the output.
Turn on the LED power supply.
2. Make sure that the discharge valve is closed and fill the manipulation area with approximately 400 ml of water.
3. Place objects in the manipulation area.
Currently, there are three possible combinations:
    * Three balls: red, yellow, and blue
    * Two triangles: red and blue
    * Three triangles: red, blue, and yellow
4. In your web browser, open the page `________________________________:5000`.
Note that the web interface should be open only on one device at a time.
Accessing the web page from multiple devices may cause unpredictable behavior.
5. Go to the drop-down menu by clicking on the button in the upper left corner and select _Balls_, _Two Triangles_, or _Three Triangles_, depending on the placed objects.
6. You can now manipulate the objects by changing the reference positions or by selecting one of the demos in the drop-down menu.
    * Change the reference positions of balls by dragging and dropping the transparent circles.
    * Change the reference of triangles by dragging and dropping the transparent triangles. Drag the center of the triangle to change the position, drag the corner of the triangle to change the orientation (see the image below).

![Changing the triangle reference](doc/triangle_reference.svg)

### Turning it off

1. Click on the _Poweroff_ button in the drop-down menu.
2. Wait until the Raspberry Pi shuts down, then unplug its power adapter.
Turn off the lab bench power supply and the LED power supply.
3. Place the hose leading from the discharge valve into a container.
Remove the manipulated objects and drain the manipulation area by opening the discharge valve.
You may need to tilt the platform in order to drain it completely.
Afterwards, wipe the manipulation area clean with paper towels.

## Reference Guide

### Accessing the web interface

In your web browser, enter the Raspberry Pi's IP address, followed by `:5000`.

If you get a "page not found" error, follow the steps in the [troubleshooting](#cannot-access-the-webinterface) section.

### Using the web interface

#### The index page and the drop-down menu

The index page serves as a _standby_ mode — it automatically disables the platform when the page is loaded.

The index page also contains the _drop-down menu_ that can be accessed by clicking the button in the upper-left corner.
The menu contains these items:

* __Platform modes__ allow switching between the manipulation of balls and triangles.
* __Computer vision service menu__ allows you to query the status of the computer vision service and to start, restart, or stop it.
Below the service control buttons is a field that displays the currently running detector.
* __Balls and triangles__ allow you to query the status of the Simulink controllers and to start, restart, or stop them.
Note that only one of these services can be running at a time.
* __Utilities__ contain three supporting functions.
  * __Calibration__ starts the semi-automatic calibration of the platform.
  ___Important: prior to the first run, you need to perform the calibration. Otherwise, the Simulink controllers will crash.___
  * __Reboot__ restarts the Raspberry Pi.
  * __Poweroff__ shuts down the Raspberry Pi.

#### Webinterface for balls

The interface can be accessed from the drop-down menu or by entering the Raspberry Pi's IP address, followed by `:5000/balls` into your browser.

The interface contains the drop-down menu and the visualization of the manipulation area.
The visualization consists of opaque and transparent circles.
The opaque circles represent the current positions of the balls.
The transparent circles represent the reference positions and can be moved by dragging and dropping.

In addition to items described in the [previous section](#the-index-page-and-the-drop-down-menu), the _drop-down_ menu also contains Demo buttons.
The Demo buttons toggle three demo modes (_i.e.,_ three different preprogrammed trajectories).
The demo mode can be deactivated by clicking on the active Demo button or by moving one of the reference circles.

#### Webinterface for triangles

The interface can be accessed from the drop-down menu or by entering the Raspberry Pi's IP address, followed by `:5000/triangle` into your browser.

The interface contains the drop-down menu and the visualization of the manipulation area.
The visualization consists of opaque and transparent triangles.
The opaque triangles represent the current positions of the objects.
The transparent triangles represent the reference positions.
You can change the reference by dragging and dropping.
Click on the center to change the position, click on the corner to change the orientation.

Similarly to the [interface for balls](#webinterface-for-balls), the _drop-down_ menu here contains one Demo button.

## Troubleshooting

### Cannot access the webinterface

#### Make sure that you have the correct IP address

If you can access the terminal on your Raspberry Pi, use the `ifconfig` command to find its IP address.

You can also find the Raspberry Pi's IP address from your computer, _e.g._, with the [Advanced IP scanner](https://www.advanced-ip-scanner.com/) tool.

#### Make sure that the webinterface service is running

1. Access a terminal on your Raspberry Pi
2. Check the status of the web interface service
```bash
systemctl status webinterface
```
3. If the field "Status" says "inactive", start the service.
```bash
sudo systemctl start webinterface
```
If the field "Status" says "failed", try to restart the service.
```bash
sudo systemctl restart webinterface
```
4. Wait a few seconds before the interface starts up, then try accessing the web page again.

### The manipulated objects jitter or are cast out of the manipulation area

#### Move the reference closer to center

If the manipulated object was cast out and now it won't return to its original position, try moving its reference position close to the center.
Then, insert the manipulated object into the center of the manipulation area and hold it in place for a few seconds with tweezers.

#### Check the settings of the computer vision service

In your web browser, enter the Raspberry Pi's IP address, followed by `:5001/ball_colors`.
Click on the Preview buttons and check if the tracked objects are properly detected.
The black and white images on the right show the results of thresholding.
The white area should fully cover the tracked objects.
If it doesn't, change the color settings and save them into the `JSON` configuration files.
If the image on the left is too bright or dark, change the `exposition_time` setting in the `JSON` configuration file.
After changing the `JSON` files, you need to restart the computer vision service.

#### Calibrate the platform

Go to the drop-down menu, select Calibration, and follow the provided instructions.

### The _Status_ button doesn't work

Access a terminal on your Raspberry Pi and restart the _webinterface_ service.
```bash
sudo systemctl restart webinterface
```
